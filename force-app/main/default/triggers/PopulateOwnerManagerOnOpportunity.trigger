trigger PopulateOwnerManagerOnOpportunity on Opportunity (before insert) {
    DatabaseHandler.populateOwnerManagerOnOpportunity(Trigger.New);
}