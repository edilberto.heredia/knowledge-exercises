trigger PotentialLeadContactDuplicate on Lead (before insert) {
    DatabaseHandler.verifyPotentialLeadContactDuplicate(Trigger.New);
}