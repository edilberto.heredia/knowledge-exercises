public with sharing class SendEmail {
    
    public static Messaging.SingleEmailMessage packEmail(String mailSender, String mailAddressee, String displayName, String subject, String body) {

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        String[] toAddresses = new String[] {mailAddressee}; 
        String[] ccAddresses = new String[] {mailSender};
        
        mail.setToAddresses(toAddresses);
        mail.setCcAddresses(ccAddresses);

        mail.setSenderDisplayName(displayName);
        mail.setSubject(subject);
        mail.setPlainTextBody(body);

        //Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        System.debug('Mail Sender: ' + mailSender);
        System.debug('Mail Addressee: ' + mailAddressee);
        System.debug('Display Name: ' + displayName);
        System.debug('Subject: ' + subject);
        System.debug('Body: ' + body);
        return mail;
    }

    public static void sendEmailToColdLead(List<Lead> leads) {
        Messaging.SingleEmailMessage email;
        String mailSender;
        String mailAddressee;
        String displayName;
        String subject;
        String body;
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        System.debug('SEND EMAIL TO COLD LEAD');
        for (Lead lead: leads) {
            if (lead.Rating != null && lead.Rating.equals('Cold')) {
                mailSender = UserInfo.getUserEmail();
                mailAddressee = lead.Email;
                if (mailAddressee != null) {
                    displayName = lead.Name;
                    subject = 'Rating Cold';
                    body = 'Changed the rating to Cold';
                    email = packEmail(mailSender, mailAddressee, displayName, subject, body);
                    emails.add(email);
                }
            }
        }
        if (emails.size() > 0) {
            sendAllEmails(emails);
        }
    }

    private static void sendAllEmails(List<Messaging.SingleEmailMessage> emails) {
        Messaging.reserveSingleEmailCapacity(emails.size());
        Messaging.sendEmail(emails);
        emails = new List<Messaging.SingleEmailMessage>();
    }
}