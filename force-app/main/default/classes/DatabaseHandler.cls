public with sharing class DatabaseHandler {

    public static void transferContact(List<Contact> contacts) {
        givePermissionsAccount();
        givePermissionsContact();
        List<Account> oktanaAccount = [SELECT Id FROM Account WHERE Name = 'Oktana Corp.'];
        if (oktanaAccount.size() != 0) {
            for (Contact c: contacts) {
                c.AccountId = oktanaAccount.get(0).Id;
            }
        }
    }

    private static void givePermissionsAccount() {
        Account.sObjectType.getDescribe().isAccessible();
        Account.sObjectType.getDescribe().isCreateable();
        Account.sObjectType.getDescribe().isUpdateable();
    }
    private static void givePermissionsContact() {
        Contact.sObjectType.getDescribe().isAccessible();
        Contact.sObjectType.getDescribe().isCreateable();
        Contact.sObjectType.getDescribe().isUpdateable();
    }
    private static void givePermissionsLead() {
        Lead.sObjectType.getDescribe().isAccessible();
        Lead.sObjectType.getDescribe().isCreateable();
        Lead.sObjectType.getDescribe().isUpdateable();
    }
    private static void givePermissionsUser() {
        User.sObjectType.getDescribe().isAccessible();
        User.sObjectType.getDescribe().isCreateable();
        User.sObjectType.getDescribe().isUpdateable();
    }
    

    public static void verifyPotentialLeadContactDuplicate(List<Lead> leads) {
        Map<String, Lead> nameToId = new Map<String, Lead>();
        List<String> names = new List<String>();
        String name;
        for (Lead lead: leads) {
            name = lead.FirstName + ' ' + lead.LastName;
            nameToId.put(name, lead);
            names.add(name);
        }
        givePermissionsContact();
        givePermissionsLead();
        List<Lead> leadsSearched = [SELECT Name FROM Lead WHERE Name in :names];
        List<Contact> contactsSearched = [SELECT Name FROM Contact WHERE Name in :names];
        List<String> duplicateNames = new List<String>();
        
        for (Lead lead: leadsSearched) {
            duplicateNames.add(lead.Name);
        }

        for(Contact contact: contactsSearched) {
            duplicateNames.add(contact.Name);
        }

        Lead duplicateLead;
        for (String duplicateName: duplicateNames) {
            duplicateLead = nameToId.get(duplicateName);
            duplicateLead.Potential_Lead_Duplicate__c = true;
        }
    }

    public static void populateOwnerManagerOnOpportunity(List<Opportunity> opps) {
        List<ID> ownerIds = new List<ID>();
        for (Opportunity op: opps) {
            ownerIds.add(op.OwnerId);
        }
        
        givePermissionsUser();
        List<User> usersWithManager = [SELECT Id, ManagerId FROM User WHERE id in :ownerIds];
        Map<Id, Id> ownerToManager = new Map<Id, Id>();
        System.debug(usersWithManager);
        for (User user: usersWithManager) {
            ownerToManager.put(user.Id, user.ManagerId);
        }

        Id ownerId;
        for (Opportunity opp: opps) {
            ownerId = opp.OwnerId;
            opp.Owner_Manager__c = ownerToManager.get(ownerId);
        }
    }
}