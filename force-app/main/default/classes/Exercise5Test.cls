@isTest
private class Exercise5Test {
    @TestSetup
    static void setup(){
        Lead lead1 = new Lead(FirstName = 'Mario', LastName = 'Paez', Company = 'Nike', Status = 'Open - Not Contacted');
        Lead lead2 = new Lead(FirstName = 'Edilberto', LastName = 'Heredia', Company = 'Oktana', Status = 'Open - Not Contacted', Email = 'edilberto_heredia@hotmail.com');
        List<Lead> leads = new List<Lead>();
        leads.add(lead1);
        leads.add(lead2);
        insert leads;
    }
    @isTest
    static void dontHaveEmailTest() {
        Test.startTest();
        Lead lead = [SELECT Id, Rating FROM Lead WHERE FirstName = 'Mario'];
        lead.Rating = 'Cold';
        update lead;
        Integer invocations = Limits.getEmailInvocations();
        Test.stopTest();
        System.assertEquals(0, invocations, 'Send the email but the lead not have it');
    }
    @isTest
    static void sendEmailTest() {
        Test.startTest();
        Lead lead = [SELECT Id, Rating FROM Lead WHERE FirstName = 'Edilberto'];
        lead.Rating = 'Cold';
        update lead;
        Integer invocations = Limits.getEmailInvocations();
        Test.stopTest();
        System.assertEquals(1, invocations, 'Dont send the email');
    }
}
