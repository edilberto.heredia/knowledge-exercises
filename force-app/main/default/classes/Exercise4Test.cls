@isTest
private class Exercise4Test {
    @isTest
    static void assignManagerIdToOpportunityTest() {
        Opportunity op = new Opportunity(Name = 'headphones sales', CloseDate = System.today(), StageName = 'Prospecting');
        insert op;
        Opportunity op2 = [SELECT Name, OwnerId, Owner_Manager__c FROM Opportunity];
        User user = [SELECT Name, ManagerId FROM User WHERE id =: UserInfo.getUserId() ];
        System.assertEquals(user.ManagerId, op2.Owner_Manager__c, 'Not assigned the manager id correctly');
    }
}
