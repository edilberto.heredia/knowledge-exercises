@isTest
private class Exercise1Test {
    @testSetup
    static void setup() {
        Account acc = new Account(Name = 'Oktana Corp.');
        insert acc;
    }
    @isTest
    static void contactUpdateAccountTest() {
        Contact contact = new Contact(FirstName = 'Peter', LastName='Fernandez');
        insert contact;
        List<Contact> contacts = [SELECT AccountId FROM Contact];
        System.assertEquals(1, contacts.size(), 'Not create a only contact');
        contact = contacts.get(0);
        List<Account> accounts = [SELECT Name FROM Account WHERE Id =: contact.AccountId];
        System.assertEquals(1, accounts.size(), 'Not linked the correct Account Id of Oktana Corp to contact');
    }
}
