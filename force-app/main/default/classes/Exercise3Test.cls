@isTest
private class Exercise3Test {
    @testSetup
    static void setup() {
        Lead lead1 = new Lead(FirstName = 'Rodrigo', LastName = 'De Paul', Company = 'Athetic Club');
        Lead lead2 = new Lead(FirstName = 'Maria', LastName = 'Rasguido', Company = 'Epson');
        List<Lead> leads = new List<Lead>();
        leads.add(lead1);
        leads.add(lead2);
        insert leads;

        Contact contact1 = new Contact(FirstName = 'Paulo', LastName = 'Dybala');
        Contact contact2 = new Contact(FirstName = 'Hugo', LastName = 'Fernandez');
        List<Contact> contacts = new List<Contact>();
        contacts.add(contact1);
        contacts.add(contact2);
        insert contacts;
    }
    @isTest
    static void verifyPotentialDuplicateNameTest() {
        Test.startTest();
        Lead lead1 = new Lead(FirstName = 'Rodrigo', LastName = 'De Paul', Company = 'Jhonson');
        Lead lead2 = new Lead(FirstName = 'Paulo', LastName = 'Dybala', Company = 'IBM');
        List<Lead> leads = new List<Lead>();
        leads.add(lead1);
        leads.add(lead2);
        insert leads;
        Test.stopTest();
        
        List<ID> ids = new List<ID>();
        ids.add(lead1.id);
        ids.add(lead2.id);
        List<Lead> leads2 = [SELECT Potential_Lead_Duplicate__c, FirstName, LastName FROM Lead WHERE ID in :ids];
        System.assertEquals(2, leads2.size(), 'Not insert correctly the 2 leads');
        System.assertEquals(true, leads2.get(0).Potential_Lead_Duplicate__c, 'Not update duplicate name');
        System.assertEquals(true, leads2.get(1).Potential_Lead_Duplicate__c, 'Not update duplicate name');
    }
}